function initMap() {
	var myCenter = {lat: 58.3782047, lng: 26.714887};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 15,
		center: myCenter
	});
	var marker = new google.maps.Marker({
		position: myCenter,
		map: map,
		animation: google.maps.Animation.BOUNCE,
		icon: "../../../bookify/images/marker.png"


	});
}


