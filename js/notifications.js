var last = "null";

function poll() {
   setTimeout(function() {
       $.ajax({ url: "http://"+location.hostname+"/bookify/api/push/"+last, success: function(data) {
            if (data.data.length!=0){
                
                if (last!="null"){                
                data.data.forEach(function(e){
                    notification_add(e.message, data.time);
                });
                }
                last = data.id;
            }
       }, dataType: "json", complete: poll }).fail(function(jqXHR, textStatus, errorThrown){

		});
    }, 1000);
};


function randomString(length) {
    var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function notification_add(m,t){
    uniqueID = randomString(16);
    var nots = document.getElementById("notifications");
    var node = document.createElement("div");
    var timeN = document.createElement("p");
    node.setAttribute("id", uniqueID);
    node.setAttribute("class", "notifMesssage");
    timeN.setAttribute("class", "time");
    timeN.innerHTML = t;
    node.innerHTML = m;
    node.appendChild(timeN);
    nots.insertBefore(node,nots.firstChild);
    setTimeout(function(){
        nots.removeChild(node);
    },
    2000)
}

poll();