var menuButtons = ["m1", "m2", "m3", "m4", "m5"];
var menuPages = ["p1", "p2", "p3", "p4", "p5"];


function btnPress(elem){
  console.log(elem);
  let c = document.getElementsByClassName("btn-select");
  for (let i = 0; i < c.length; i++) {
    c[i].className = "btn";
  }
  elem.className = "btn-select";
  swapContent(menuPages[menuButtons.indexOf(elem.getAttribute("id"))]);
}

function onLoadBody(){
  for (let i = 0; i < menuButtons.length; i++) {
    let elem = document.getElementById(menuButtons[i]); 
    elem.addEventListener('click', function(){btnPress(elem)}, false);
  }
  btnPress(document.getElementById("m1"));
}

function swapContent(e){
  let elem = document.getElementById("content");
  elem.innerHTML = document.getElementById(e).innerHTML;
}
