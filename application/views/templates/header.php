<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bookify</title>

		<script
			  src="https://code.jquery.com/jquery-3.2.1.min.js"
			  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
			  crossorigin="anonymous">
		</script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<?php if (!empty(cdn('css/style_123123.css'))) : ?>
			<link href="<?php echo cdn('css/style_123123.css'); ?>" rel="stylesheet"/>
		<?php else : ?>
			<link href="<?php echo base_url(); ?>css/style_123123.css" rel="stylesheet"/>
		<?php endif; ?>

	</head>
	<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo base_url(); ?>">Bookify</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo base_url(); ?>broneeri"><span title="<?php echo lang("header_book_"); ?>"><?php echo lang("header_book"); ?></span></a></li>
					<li><a href="<?php echo base_url(); ?>partnerid"><span title="<?php echo lang("header_partners_"); ?>"><?php echo lang("header_partners"); ?></span></a></li>
					<li><a href="<?php echo base_url(); ?>kontakt"><span title="<?php echo lang("header_contact_"); ?>"><?php echo lang("header_contact"); ?></span></a></li>
					<li><a href="<?php echo base_url(); ?>profile"><span title="<?php echo lang("header_profile_"); ?>"><?php echo lang("header_profile"); ?></span></a></li>


				</ul>
				<ul class="nav navbar-nav navbar-right">
					<?php if (  $this->facebook->is_authenticated()) { ?>
						<a href="<?php echo $this->facebook->logout_url(); ?>" title="Facebook" class="btn btn-facebook btn-lg"><i class="fa fa-facebook fa-fw"></i> Log out</a>
						<?php
					}?>

					<?php if (isset($_SESSION['username'])) { ?>

						<li><a href="authentications/logout"><?php echo lang("header_logout"); ?></a></li>
						<?php
					}?>
					<?php if (!isset($_SESSION['username']) && !($this->facebook->is_authenticated())) { ?>
						<li><a href="<?php echo base_url(); ?>login"><?php echo lang("header_login"); ?></a></li>
						<li><a href="<?php echo base_url(); ?>register"><?php echo lang("header_register"); ?></a></li>
						<?php
					}?>
					<li><a href="<?php echo base_url(); ?>index.php/pages/vahetaKeelt/estonian"><span title="<?php echo lang("header_est"); ?>"><img class="lipp" alt="estonian flag" src="<?php echo base_url(); ?>images/est.png"/></span></a></li>
					<li><a href="<?php echo base_url(); ?>index.php/pages/vahetaKeelt/english"><span title="<?php echo lang("header_eng"); ?>"><img class="lipp" alt="union jack" src="<?php echo base_url(); ?>images/gb.png"/></span></a></li>
					<?php if (isset($_SESSION['username'])){?>
						<li><a href="<?php echo base_url(); ?>profile"><?php echo $_SESSION['username']?></a></li>
					<?php }?>
				</ul>
			</div>
		</div>
	</nav>
