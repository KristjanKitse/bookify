<div class="col-lg-5 col-lg-offset-2">
	<h2><?= $title ;?></h2>

	<?php echo validation_errors(); ?>

	<?php echo form_open_multipart('authentications/register'); ?>
		<div class="form-group">
			<label><?php echo lang("authentications_reg_name"); ?></label>
			<input type="text" class="form-control" name="name"
			placeholder="<?php echo lang("authentications_reg_nametext");?>">
		</div>
		<div class="form-group">
			<label><?php echo lang("authentications_reg_username"); ?></label>
			<input type="text" class="form-control" name="username"
		   	placeholder="<?php echo lang("authentications_reg_usernametext");?>">
		</div>
		<div class="form-group">
			<label><?php echo lang("authentications_reg_pass"); ?></label>
			<input type="password" class="form-control" name="password"
		  	 placeholder="<?php echo lang("authentications_reg_passtext");?>">
		</div>
		<div class="form-group">
			<label><?php echo lang("authentications_reg_confirmpass"); ?></label>
			<input type="password" class="form-control" name="repeatpass"
		  	 placeholder="<?php echo lang("authentications_reg_passtext");?>">
		</div>
		<div class="form-group">
			<label><?php echo lang("authentications_reg_email"); ?></label>
			<input type="text" class="form-control" name="email"
		  	 placeholder="<?php echo lang("authentications_reg_emailtext");?>">
		</div>
		<div class="form-group">
			<label for="sex"><?php echo lang("authentications_reg_sex"); ?></label>
			<select id="sex" type="text" class="form-control" name="sex">
				<option value="male"><?php echo lang("authentications_reg_sexmale"); ?></option>
				<option value="female"><?php echo lang("authentications_reg_sexfemale"); ?></option>
			</select>
		</div>


		<button type="submit" class="btn btn-default"><?php echo lang("authentications_reg_reg");?></button>

		</form>
</div>

