<div class="col-lg-5 col-lg-offset-2">


<h2><?= $title ;?></h2>

	<?php if (isset($_SESSION['login_error'])) { ?>
		<?php echo lang("login_error")?>
		<?php
	}?>

<?php echo validation_errors(); ?>

<?php echo form_open_multipart('authentications/login'); ?>
<div class="form-group">
	<label><?php echo lang("authentications_reg_username"); ?></label>
	<input type="text" class="form-control" name="username"
		   placeholder="<?php echo lang("authentications_reg_usernametext");?>">
</div>
<div class="form-group">
	<label><?php echo lang("authentications_reg_pass"); ?></label>
	<input type="password" class="form-control" name="password"
		   placeholder="<?php echo lang("authentications_reg_passtext");?>">
</div>


<button type="submit" class="btn btn-default"><?php echo lang("authentications_login");?></button>
	<?php if ( ! $this->facebook->is_authenticated()) : ?>
		<a href="<?php echo $this->facebook->login_url(); ?>" title="Facebook" class="btn btn-facebook btn-lg"><i class="fa fa-facebook fa-fw"></i> Facebook</a>
	<?php endif; ?>




</form>
</div>
