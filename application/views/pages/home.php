
<div id="p1">
	<div class="page">
		<h1>Bookify</h1>
		<h2><?php echo lang("home_msg"); ?></h2>
		<?php if (isset($_SESSION['registration_success'])) { ?>
			<div class="alert alert-success"> <?php echo lang("registration_success")?></div>
			<?php
		}?>
		<?php if (isset($_SESSION['login_success'])) { ?>
			<div class="alert alert-success"> <?php echo lang("login_success")?>, <?php echo $_SESSION['username']?></div>
			<?php
		}?>
	</div>
</div>
