<div id="p2">
	<div class="page">
		<h1><?php echo lang("header_book"); ?></h1>
		<h2><?php echo lang("book_msg"); ?></h2>
		<div class="bron-tabel">
			<table id = "broneerimata">
				<tr>
					<th><?php echo lang("book_table_firm"); ?></th>
					<th><?php echo lang("book_table_type"); ?></th>
					<th><?php echo lang("book_table_service"); ?></th>
					<th><?php echo lang("book_table_price"); ?></th>
					<th><?php echo lang("header_book"); ?></th>
				</tr>
			</table>
		</div>
		<a href="#" id="load_more_link" onclick="load_more();">
		<div class="button" id="load_more_nupp">
			<?php echo lang("book_table_load_more"); ?>
		</div>
		</a>
	</div>
</div>


<?php
$lang = $this->session->userdata('site_lang');
if (!$lang) {
    $this->lang->load('myappl', 'estonian');
    $this->config->set_item('language', 'estonian');
	$lang = 'estonian';
}
?>
<script>
	var template = `
	<tr><td>%firma%</td>
	<td>%type%</td>
<td>%teenus%</td>
<td>%hind%</td>
<td><a href="#" onclick="fBron('%kasutaja%', '%hind%')">%bron%</a></td>
</tr>`
	var c = 0;
	var by = 8;
	function load_more(){
		jQuery.getJSON("api/"+c+"/"+by, function(d){
			if (d.count!=0){
				c+=d.count;
				d.rows.forEach(function(e){
					add_to_table(e);
				});
				return null;
			}
			document.getElementById("load_more_nupp").remove();
			document.getElementById("load_more_link").remove();
		}).fail(function(jqXHR, textStatus, errorThrown){
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			console.log("api/"+c+"/"+by);
			document.getElementById("load_more_nupp").remove();
			document.getElementById("load_more_link").remove();
		});
	}
	function add_to_table(data){
		var keel = "<?php echo $lang?>";
		var bron = "Book";
		if (keel == "estonian"){
			bron = "Broneeri";
		}
		var tabel = document.getElementById("broneerimata").children[0];
		var templ = template;
		tabel.innerHTML+=templ.replace("%firma%",data.firma).replace("%type%",data.type)
		.replace("%teenus%",data.teenus).replace("%hind%",data.hind).replace("%bron%",bron)
		.replace("%kasutaja%", "<?php if(isset($_SESSION['username'])){
			echo $_SESSION['username'];
		}else{
			echo "Guest user";
		}?>").replace("%hind%", data.hind.replace("€",""));
	}

	function fBron(kasutaja, hind){
		console.log("<?php echo base_url()?>"+"api/post/"+kasutaja+"/"+hind);
		$.post("<?php echo base_url()?>"+"api/post/"+kasutaja+"/"+hind, {xml: ""});
	}

	load_more();
</script>
