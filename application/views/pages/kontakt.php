<div id="p4" xmlns:https="http://www.w3.org/1999/xhtml">
	<div class="page">
		<h1><?php echo lang("header_contact"); ?></h1>
		<h2><?php echo lang("contact_msg"); ?></h2>
		<div class="kontakt">
			<div class="kolmandik">
				<img alt="" class="img-circle" src="<?php echo base_url('images/karl.jpg'); ?>"/>
				<p>Karl-Jaak Maaroos</p>
				<p>karl-jaak.maaroos@gmail.com</p>
			</div>
			<div class="kolmandik">
				<img alt="" class="img-circle" src="<?php echo base_url('images/kristjan.jpg'); ?>"/>
				<p>Kristjan Kitse</p>
				<p>kristjan.kitse@gmail.com</p>
			</div>
			<div class="kolmandik">
				<img alt="" class="img-circle" src="<?php echo base_url('images/verner.jpg'); ?>"/>
				<p>Verner Läll</p>
				<p>verner.lall@gmail.com</p>
			</div>


			<!DOCTYPE html>
			<html>
			<head>
				<style>
					#map {
						height: 400px;
						width: 100%;
					}
				</style>
			</head>
			<body>
			<div id="map"></div>
			<script src="<?php echo base_url('js/map.js'); ?>"></script>
			<script async defer
					src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdwQrR66Mvy6fK5OM5fT1P84M5qObsPRQ&callback=initMap">
			</script>
			</body>




		</div>
	</div>
</div>
