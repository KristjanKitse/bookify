<div id="p3">
	<div class="page">
		<h1><?php echo lang("header_partners"); ?></h1>
		<div class="kontakt">


<?php 

	if ($firma_item === "undefined"){
	foreach ($firmad as $firma_item): ?>		
		<div class="firma">
			<div class="firma-logo">
				<img alt="" class="img-firma-logo" src="<?php echo base_url('images/'.$firma_item['imageFile']); ?>"/>
			</div>
			<div class="right">
				<h2><?php echo $firma_item['companyName'];?></h2>
				<br/>
				<p><a href="<?php echo site_url('partnerid/'.$firma_item['id']); ?>"><?php echo lang("parterns_more_info"); ?></a></p>
			</div>
			<div class="clearfix"></div>
		</div>

<?php endforeach; } else {?>
		<div class="firma">
			<div class="firma-logo">
				<img alt="" class="img-firma-logo" src="<?php echo base_url('images/'.$firma_item['imageFile']); ?>"/>
			</div>
			<div class="right">
				<h2><?php echo $firma_item['companyName'];?></h2>
				<p><?php echo $firma_item['description']; ?></p>
				<br/>
				<p><a href="<?php echo site_url('partnerid/'); ?>"><?php echo lang("parterns_more_info_back"); ?></a></p>
			</div>
			<div class="clearfix"></div>
		</div>
<?php }?>

		</div>
	</div>
</div>
