<?php
class Firmad_model extends CI_Model {
    
    public function __construct(){
        $this->load->database();
    }

    public function get_firmad($id = "all"){
        $lang = $this->session->userdata('site_lang');
        if (!$lang) {
            $this->lang->load('myappl', 'estonian');
			$this->config->set_item('language', 'estonian');
            $lang = 'estonian';
		}


        if ($id === "all"){
            if ($lang === 'estonian'){
                $query = $this->db->get_where("view_et", array('showEntry' => 1));
            } 
            if ($lang === 'english'){
                $query = $this->db->get_where("view_en", array('showEntry' => 1));
            } 
            
            return $query->result_array();
        }
        
            if ($lang === 'estonian'){
                $query = $this->db->get_where("view_et", array('id' => $id));
            } 
            if ($lang === 'english'){
                $query = $this->db->get_where("view_en", array('id' => $id));
            } 
        return $query->row_array();
    }
}
?>