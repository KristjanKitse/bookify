<?php
class API_model extends CI_Model {
    
    public function __construct(){
        $this->load->database();
    }

    public function add_notification($message, $user){
			$data = array(
				'message' => $message,
				'user' => $user
			);
			return $this->db->insert('t_notifications',$data);
		}

    public function get_more($from, $count){

        $eesti = array(
            array("firma" => "Verru ilusalong",
            "type" => "Juuksur",
            "hind" => "5€",
            "teenus" => "Meeste juustelõikus"),
            array("firma" => "Verru ilusalong",
            "type" => "Juuksur",
            "hind" => "15€",
            "teenus" => "Naiste juustelõikus"),
            array("firma" => "Kitse autotöökoda",
            "type" => "Auto remont",
            "hind" => "20€",
            "teenus" => "Kojameeste vahetus"),
            array("firma" => "Kitse autotöökoda",
            "type" => "Auto remont",
            "hind" => "100€",
            "teenus" => "Pihusti vahetus"),
            array("firma" => "Kitse autotöökoda",
            "type" => "Auto remont",
            "hind" => "1000€",
            "teenus" => "Plokikaane vahetus"),
            array("firma" => "Kitse autotöökoda",
            "type" => "Auto remont",
            "hind" => "200€",
            "teenus" => "Bensiinipaagi keevitus"),
            array("firma" => "Maaroosi kohvik",
            "type" => "Restoran",
            "hind" => "10€",
            "teenus" => "Laud kahele"),
            array("firma" => "Maaroosi kohvik",
            "type" => "Restoran",
            "hind" => "12€",
            "teenus" => "Laud kolmele"),
            array("firma" => "Maaroosi kohvik",
            "type" => "Restoran",
            "hind" => "25€",
            "teenus" => "Suur laud (kuni 10 inimest)")
        );
        $inglise = array(
            array("firma" => "Verru beauty saloon",
            "type" => "Hairdresser",
            "hind" => "5€",
            "teenus" => "Mens haircut"),
            array("firma" => "Verru beauty saloon",
            "type" => "Hairdresser",
            "hind" => "15€",
            "teenus" => "Women's haircut"),
            array("firma" => "Kitse car workshop",
            "type" => "Car repairs",
            "hind" => "20€",
            "teenus" => "Wipers change"),
            array("firma" => "Kitse car workshop",
            "type" => "Car repairs",
            "hind" => "100€",
            "teenus" => "Piston change"),
            array("firma" => "Kitse car workshop",
            "type" => "Car repairs",
            "hind" => "1000€",
            "teenus" => "Engine block change"),
            array("firma" => "Kitse car workshop",
            "type" => "Car repairs",
            "hind" => "200€",
            "teenus" => "Welding of gas tank"),
            array("firma" => "Maaroosi café",
            "type" => "Restoran",
            "hind" => "10€",
            "teenus" => "Table for two"),
            array("firma" => "Maaroosi café",
            "type" => "Restoran",
            "hind" => "12€",
            "teenus" => "Table for three"),
            array("firma" => "Maaroosi café",
            "type" => "Restoran",
            "hind" => "25€",
            "teenus" => "Big table (up to 10 people)")
        );
        $lang = $this->session->userdata('site_lang');

        if (!$lang) {
            $this->lang->load('myappl', 'estonian');
			$this->config->set_item('language', 'estonian');
            $lang = 'estonian';
		}
        $kasuta = $eesti;
        if ($lang==="estonian"){
            $kasuta = $eesti;
        } else if ($lang === "english"){
            $kasuta = $inglise;
        }

        $r = array();
        for ($i=$from; $i<count($kasuta); $i++){
            array_push($r, $kasuta[$i]);
            if ($i-$from==$count-1){
                break;
            }
        }
        return $r;
    }


}
?>