<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Header
$lang['header_book'] = 'Book';
$lang['header_contact'] = 'Contact';
$lang['header_submit'] = 'Submit';
$lang['header_partners'] = 'Partners';
$lang['header_login'] = 'Login';
$lang['header_register'] = 'Sign up';
$lang['header_profile'] = 'Profile';
$lang['header_est'] = 'Estonian';
$lang['header_eng'] = 'English';
$lang['header_profile_'] = 'Here you can watch your profile';
$lang['header_book_'] = 'Here you can make reservations';
$lang['header_contact_'] = 'Who are we?';
$lang['header_partners_'] = 'Here you can see our partners';



// Book
$lang['book_msg'] = 'Click "book" to open calendar and pick a suitable date';
$lang['book_table_firm'] = 'Company';
$lang['book_table_type'] = 'Type';
$lang['book_table_price'] = 'Price';
$lang['book_table_service'] = 'Service';
$lang['book_table_load_more'] = 'Load more';

//Home
$lang['home_msg'] = 'All your reservations in one place';

// Contact
$lang['contact_msg'] = 'We are located on J. Liivi 2, Tartu';

// Partnerid
$lang['parterns_more_info'] = "More details";
$lang['parterns_more_info_back'] = "Back to the list";

//Authentication
$lang['authentications_register'] = 'Creating an user';
$lang['authentications_reg_name'] = 'Name';
$lang['authentications_reg_nametext'] = 'Enter name';
$lang['authentications_reg_reg'] = 'Sign up';
$lang['authentications_reg_username'] = 'Username';
$lang['authentications_reg_usernametext'] = 'Enter username';
$lang['authentications_reg_pass'] = 'Password';
$lang['authentications_reg_passtext'] = 'Enter password';
$lang['authentications_reg_confirmpass'] = 'Confirm password';
$lang['authentications_reg_email'] = 'Email';
$lang['authentications_reg_emailtext'] = 'Enter email';
$lang['authentications_reg_sex'] = 'Sex';
$lang['authentications_reg_sexmale'] = 'Male';
$lang['authentications_reg_sexfemale'] = 'Female';
$lang['authentications_login'] = 'Login';

//Sucesses
$lang['registration_success'] = 'Your account was succesfully registered. You can log in now.';
$lang['login_success'] = 'You are logged in';
$lang['login_error'] = 'Login authentication failed.';
$lang['header_logout'] = 'Log out';

//Profile
$lang['profile_header'] = 'Profile';
$lang['profile_name'] = 'Name';
$lang['profile_username'] = 'Username';
$lang['profile_email'] = 'Email';
$lang['noprofile_info'] = 'You have to log in to view the page!';


