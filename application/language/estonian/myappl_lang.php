<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Header
$lang['header_book'] = 'Broneeri';
$lang['header_contact'] = 'Kontakt';
$lang['header_submit'] = 'Otsi';
$lang['header_partners'] = 'Partnerid';
$lang['header_login'] = 'Logi sisse';
$lang['header_register'] = 'Loo kasutaja';
$lang['header_profile'] = 'Profiil';
$lang['header_est'] = 'Eesti keel';
$lang['header_eng'] = 'Inglise keel';
$lang['header_profile_'] = 'Siit saad vaadata oma profiili';
$lang['header_book_'] = 'Siin saad teha broneeringuid';
$lang['header_contact_'] = 'Kes me oleme?';
$lang['header_partners_'] = 'Siit saad näha meie partnereid';


// Book
$lang['book_msg'] = 'Vajuta "broneeri" et avada kalender ja valida kuupäev';
$lang['book_table_firm'] = 'Firma';
$lang['book_table_type'] = 'Tüüp';
$lang['book_table_price'] = 'Hind';
$lang['book_table_service'] = 'Teenus';
$lang['book_table_load_more'] = 'Lae veel';

// Home
$lang['home_msg'] = 'Kõik sinu broneeringud ühest kohast';

// Contact
$lang['contact_msg'] = 'Asume J. Liivi 2, Tartu';

// Partnerid
$lang['parterns_more_info'] = "Vaata lähemalt";
$lang['parterns_more_info_back'] = "Tagasi nimekirja";

//Authentication
$lang['authentications_register'] = 'Kasutaja loomine';
$lang['authentications_reg_name'] = 'Nimi';
$lang['authentications_reg_nametext'] = 'Sisesta nimi';
$lang['authentications_reg_reg'] = 'Loo kasutaja';
$lang['authentications_reg_username'] = 'Kasutajatunnus';
$lang['authentications_reg_usernametext'] = 'Sisesta kasutajatunnus';
$lang['authentications_reg_pass'] = 'Salasõna';
$lang['authentications_reg_passtext'] = 'Sisesta salasõna';
$lang['authentications_reg_confirmpass'] = 'Korda salasõna';
$lang['authentications_reg_email'] = 'Email';
$lang['authentications_reg_emailtext'] = 'Sisesta email';
$lang['authentications_reg_sex'] = 'Sugu';
$lang['authentications_reg_sexmale'] = 'Mees';
$lang['authentications_reg_sexfemale'] = 'Naine';
$lang['authentications_login'] = 'Logi sisse';



//Home page flashdatas
$lang['registration_success'] = 'Oled edukalt registreeritud. Võid nüüd sisse logida';
$lang['login_success'] = 'Oled sisse logitud';
$lang['login_error'] = 'Sisselogimine ebaõnnestus';
$lang['header_logout'] = 'Logi välja';

//Profile
$lang['profile_header'] = 'Profiil';
$lang['profile_name'] = 'Nimi';
$lang['profile_username'] = 'Kasutajatunnus';
$lang['profile_email'] = 'Email';
$lang['noprofile_info'] = 'Peate lehe vaatamiseks sisse logima!';
