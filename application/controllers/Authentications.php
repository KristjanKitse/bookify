<?php

class Authentications extends CI_Controller{


	public function register(){
		$data['title'] = lang("authentications_register");

		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required|min_length[5]');
		$this->form_validation->set_rules('repeatpass','Confirm password','required|matches[password]');
		$this->form_validation->set_rules('email','Email','required|valid_email');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('authentications/register',$data);
			$this->load->view('templates/footer');
		}else{
			$this->authentication_model->create_user();
			$this->session->set_flashdata("registration_success", "");
			redirect(base_url(),"refresh");
		}
	}

	public function login(){
		$data['title'] = lang("authentications_login");

		$this->form_validation->set_rules('username','Username','required');
		$this->form_validation->set_rules('password','Password','required');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header');
			$this->load->view('authentications/login',$data);
			$this->load->view('templates/footer');
		}
		else{
			$user=$this->authentication_model->authenticate_user();
			if(is_null($user)){
				$this->session->set_flashdata("login_error","user doesn't exist");
				redirect('authentications/login',"refresh");
			}
			else{
				$this->session->set_flashdata("login_success","you are now logged in");

				$_SESSION['user_logged'] = TRUE;
				$_SESSION['username'] = $user->username;
				$_SESSION['email'] = $user->email;
				$_SESSION['name'] = $user->name;


				$this->session->set_userdata(TRUE,$_SESSION['username']);

				redirect('profile');
			}
		}
	}

	public function logout(){
		$this->session->unset_userdata($_SESSION['username']);
		session_destroy();
		redirect(base_url(), 'refresh');
	}

	public function fblogin()
	{
		$data['user'] = array();
		$data['title'] = lang("authentications_login");
		// Check if user is logged in
		if ($this->facebook->is_authenticated())
		{
			// User logged in, get user details
			$user = $this->facebook->request('get', '/me?fields=id,name,email');
			if (!isset($user['error']))
			{
				$data['user'] = $user;
			}
		}
		// display view
		$this->load->view('templates/header', $data);
		$this->load->view('secretpages/profile', $data);
		$this->load->view('templates/footer', $data);
	}

	public function fblogout()
	{
		$this->facebook->destroy_session();
		redirect('');
	}





}
