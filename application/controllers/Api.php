<?php
class Api extends CI_Controller {
    
    public function __construct()
        {
            parent::__construct();
            $this->load->model('API_model');
            $this->load->helper('url_helper');
        }

    public function get($id=FALSE, $many=FALSE){
        if ($id===FALSE or $many===FALSE){
            $this->output->set_content_type('text/html')
                    ->set_output("Wrong use of the API.");
            return;
        }
        $rows = $this->API_model->get_more($id, $many);
        $rowcount = count($rows);
        if ($rowcount==0){
            $output = array('count' => $rowcount,
                    'rows' => array()
                );
        } else {
        $output = array('count' => $rowcount,
                    'rows' => $rows
                );
        }
        $this->output->set_content_type('application/json; charset=utf-8')
                    ->set_output(json_encode($output));
    }
    
    public function post($user, $h){
        $u = str_replace("%20", " ", $user);
        $message = $u." just booked something in value of ".$h."€";
        $this->API_model->add_notification($message, $u);
    }

    public function push($last){
    $r=true;
    $count = 0;
    while ($r===true && $count<10) {
            if ($last == "null"){
                $this->db->limit(1);
                $this->db->order_by("id", "desc");
            }
            else {
                $i = intval($last);
                $this->db->where('id >', $i);
                $this->db->order_by("id", "desc");
                $this->db->limit(3);
            }
            $query = $this->db->get("t_notifications");
            $result = $query->result_array();
            if (count($result)>0){
                $r = $result;
            } else $r = true;
            usleep(300);
            $count++;   
        }

        $output = array('time' => date("h:i:s"),
                    'id' => array_column($r, "id")[0],
                    'data' => $r
                );
        $this->output->set_content_type('application/json; charset=utf-8')
                ->set_output(json_encode($output));
    
    }
}
?>
