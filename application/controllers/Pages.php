<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kristjan
 * Date: 3/12/2017
 * Time: 7:56 PM
 */
class Pages extends CI_Controller {

	public function view($page = 'home')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		$data['title'] = ucfirst($page); // Capitalize the first letter

		if(isset($_SESSION['username'])){
			$data['username'] = $_SESSION['username'];
			$this->load->view('templates/header', $data);
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}
		else{
			$this->load->view('templates/header', $data);
			$this->load->view('pages/'.$page, $data);
			$this->load->view('templates/footer', $data);
		}


	}

	function vahetaKeelt($language = "") {

		if($language == "")
			$language = "estonian";

		$this->session->set_userdata('site_lang', $language);
		redirect(base_url());
	}


}
?>