<?php
/**
 * Created by IntelliJ IDEA.
 * User: karl-jaak
 * Date: 09/04/2017
 * Time: 13:21
 */
class Secretpages extends CI_Controller {

	public function view($page = 'home')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		$data['title'] = ucfirst($page); // Capitalize the first letter

		if(isset($_SESSION['username'])){
			$data['username'] = $_SESSION['username'];
			$this->load->view('templates/header', $data);
			$this->load->view('secretpages/profile');
			$this->load->view('templates/footer', $data);
		}
		else if($this->facebook->is_authenticated()){
			$data['user'] = array();
			$user = $this->facebook->request('get', '/me?fields=id,name,email');
			$data['user'] = $user;
			$this->load->view('templates/header', $data);
			$this->load->view('secretpages/profile');
			$this->load->view('templates/footer', $data);
		}
		else{
			$this->load->view('templates/header', $data);
			$this->load->view('authentications/login');
			$this->load->view('templates/footer', $data);
		}


	}

	function vahetaKeelt($language = "") {

		if($language == "")
			$language = "estonian";

		$this->session->set_userdata('site_lang', $language);
		redirect(base_url());
	}


}
