<?php
class Firmad extends CI_Controller {
    
    public function __construct()
        {
            parent::__construct();
            $this->load->model('Firmad_model');
            $this->load->helper('url_helper');
        }

    public function view($id = FALSE){

        $data['title'] = lang("header_partners");

        if ($id==="all"){
            $data['firmad'] = $this->Firmad_model->get_firmad($id);
            $data['firma_item'] = "undefined";
            }
        else{
            $data['firma_item'] = $this->Firmad_model->get_firmad($id);
            }
        
        $this->load->view('templates/header',  $data);
        $this->load->view('pages/partnerid', $data);
        $this->load->view('templates/footer', $data);
    }
}
?>
